//Khai báo mongoose 
const mongoose = require ('mongoose');
//Khai báo schema
const Schema = mongoose.Schema;
//Tạo Schema user
const userSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true,
        unique: true
    },
    age: {
        type: Number,
        default: 0
    },
    cars: [{
        type: mongoose.Types.ObjectId,
        ref: 'car'
    }]
})

module.exports = mongoose.model('user', userSchema);