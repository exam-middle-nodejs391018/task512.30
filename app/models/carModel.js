//Khai báo mongoose
const mongoose = require('mongoose');
//Khai báo Schema
const Schema = mongoose.Schema;
//Tạo car schema
const carSchema = new Schema({
    model: {
        type: String,
        require: true
    },
    vld: {
        type: String,
        require: true,
        unique: true
    }
})

module.exports = mongoose.model('car', carSchema)