//Khia báo express
const express = require ('express');
//Khai báo port
const port = 8000;
//Tạo app
const app = express();
//Import data
const {userObject} = require('./data')
app.get('/', (req, res) => {
    res.status(200).send({
        message: `App running in port ${port}`
    })
})

app.get('/users', (req, res) => {
    let age = req.query.age
    console.log(age)
    let userSortAge = []
    let sortAge = false;
    for (let i = 0; i < userObject.length; i++) {
        if (userObject[i].age.toString() >= age) {
            sortAge = true;
            userSortAge.push(userObject[i])
        }
    }
    if (sortAge) {
        res.status(200).json({
            message: `Age is than or equal ${age}`,
            data: userSortAge
        })
    } else {
        res.status(400).json({
            message: `Not age is than or equal ${age}`,
            data: userObject
        })
    }
})

app.get('/users/:userId', (req, res) => {
    let userId = req.params.userId;
    console.log(userId)
    let foundUser = false;
    for(let i = 0; i < userObject.length; i++) {
        if(userObject[i].id.toString() === userId) {
            foundUser = true
            res.status(200).json(userObject[i])
            break;
        } 
    }
    if(!foundUser) {
        res.status(400).json({
            message: 'User not found'
        })
    }
    
})

app.listen(port, () => {
    console.log('Server running at http://localhost:' + port);
})