class User {
    constructor (id, name, position, office, age, startDate) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.office = office;
        this.age = age;
        this.startDate = startDate;
    }
}
let userObject = []

let User1 = new User (1, 'Airi Satou', 'Accountant', 'Tokyo', 33, '2008/11/28')
userObject.push(User1);

let User2 = new User (2, 'Angelica Ramos', 'Chief Executive Officer (CEO)', 'London', 47, '2009/10/09')
userObject.push(User2);

let User3 = new User (3, 'Ashton Cox', 'Junior Technical Author', 'San Francisco', 66, '2009/01/12')
userObject.push(User3);

let User4 = new User (4, 'Bradley Greer', 'Software Engineer', 'London', 44, '2012/10/13')
userObject.push(User4);

let User5 = new User (5, 'Brenden Wagner', 'Software Engineer', 'San Francisco', 28, '22011/06/07')
userObject.push(User5);

let User6 = new User (6, 'Brielle Williamson', 'Integration Specialist', 'New York', 61, '2012/12/02')
userObject.push(User6);

let User7 = new User (7, 'Bruno Nash', 'Software Engineer', 'London', 38, '2011/05/03')
userObject.push(User7);

let User8 = new User (8, 'Caesar Vance', 'Pre-Sales Support', 'New York', 21 ,'2011/12/12')
userObject.push(User8);

let User9 = new User (9, 'Cara Stevens', 'Sales Assistant', 'New York', 46, '2011/12/06')
userObject.push(User9);

let User10 = new User (10, 'Cedric Kelly', 'Senior Javascript Developer', 'Edinburgh', 22, '2012/03/29')
userObject.push(User10);

module.exports = {userObject}
